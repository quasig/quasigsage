from sage.all import *


def hex_to_bin(hx):
    return bin(int(hx, 16))[2:]


def bin_to_finite_field(bn, field, fill=0):
    degree = field.degree()
    bn = bn.zfill(fill)
    bins = [bn[i * degree:  degree * (i + 1)] for i in range(len(bn) // degree)]

    def bn_to_gf(b):
        return field([int(_) for _ in b[::-1]])

    return vector(field, [bn_to_gf(b) for b in bins] if degree > 1 else [int(b) for b in bins])


def hex_to_finite_field(hx, field, fill_bits=0):
    return bin_to_finite_field(hex_to_bin(hx), field, fill=fill_bits)


def finite_field_to_bin(ff, field, fill=0):
    degree = field.degree()
    res = ''
    if degree == 1:
        res = res.join([str(i) for i in ff])
    else:
        res = res.join([bin(f.integer_representation())[2:].zfill(degree) for f in ff])

    return res.zfill(fill)


def finite_field_to_hex(ff, field, fill_bits=0):
    return format((int(finite_field_to_bin(ff, field, fill=fill_bits), 2)), 'x')
