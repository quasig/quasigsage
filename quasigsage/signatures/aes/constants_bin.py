from sage.matrix.all import matrix
from sage.matrix.special import block_matrix
from sage.rings.finite_rings.all import GF

__sr1 = block_matrix(GF(2), [[matrix.identity(8), matrix.zero(8, 15 * 8)]])
__sr2 = block_matrix(GF(2), [[matrix.zero(8, 13 * 8), matrix.identity(8), matrix.zero(8, 2 * 8)]])
__sr3 = block_matrix(GF(2), [[matrix.zero(8, 10 * 8), matrix.identity(8), matrix.zero(8, 5 * 8)]])
__sr4 = block_matrix(GF(2), [[matrix.zero(8, 7 * 8), matrix.identity(8), matrix.zero(8, 8 * 8)]])
__sr5 = block_matrix(GF(2), [[matrix.zero(8, 4 * 8), matrix.identity(8), matrix.zero(8, 11 * 8)]])
__sr6 = block_matrix(GF(2), [[matrix.zero(8, 1 * 8), matrix.identity(8), matrix.zero(8, 14 * 8)]])
__sr7 = block_matrix(GF(2), [[matrix.zero(8, 14 * 8), matrix.identity(8), matrix.zero(8, 1 * 8)]])
__sr8 = block_matrix(GF(2), [[matrix.zero(8, 11 * 8), matrix.identity(8), matrix.zero(8, 4 * 8)]])
__sr9 = block_matrix(GF(2), [[matrix.zero(8, 8 * 8), matrix.identity(8), matrix.zero(8, 7 * 8)]])
__sr10 = block_matrix(GF(2), [[matrix.zero(8, 5 * 8), matrix.identity(8), matrix.zero(8, 10 * 8)]])
__sr11 = block_matrix(GF(2), [[matrix.zero(8, 2 * 8), matrix.identity(8), matrix.zero(8, 13 * 8)]])
__sr12 = block_matrix(GF(2), [[matrix.zero(8, 15 * 8), matrix.identity(8)]])
__sr13 = block_matrix(GF(2), [[matrix.zero(8, 12 * 8), matrix.identity(8), matrix.zero(8, 3 * 8)]])
__sr14 = block_matrix(GF(2), [[matrix.zero(8, 9 * 8), matrix.identity(8), matrix.zero(8, 6 * 8)]])
__sr15 = block_matrix(GF(2), [[matrix.zero(8, 6 * 8), matrix.identity(8), matrix.zero(8, 9 * 8)]])
__sr16 = block_matrix(GF(2), [[matrix.zero(8, 3 * 8), matrix.identity(8), matrix.zero(8, 12 * 8)]])

_SHIFT_ROW_MATRIX = block_matrix(GF(2),
                                 [
                                     [__sr1],
                                     [__sr2],
                                     [__sr3],
                                     [__sr4],
                                     [__sr5],
                                     [__sr6],
                                     [__sr7],
                                     [__sr8],
                                     [__sr9],
                                     [__sr10],
                                     [__sr11],
                                     [__sr12],
                                     [__sr13],
                                     [__sr14],
                                     [__sr15],
                                     [__sr16]
                                 ]
                                 )

__mc1 = matrix(GF(2), matrix.identity(8))
__mc2 = matrix(GF(2),
               [
                   [0, 0, 0, 1, 1, 0, 1, 1],
                   [1, 0, 0, 0, 0, 0, 0, 0],
                   [0, 1, 0, 0, 0, 0, 0, 0],
                   [0, 0, 1, 0, 0, 0, 0, 0],
                   [0, 0, 0, 1, 0, 0, 0, 0],
                   [0, 0, 0, 0, 1, 0, 0, 0],
                   [0, 0, 0, 0, 0, 1, 0, 0],
                   [0, 0, 0, 0, 0, 0, 1, 0],
               ]
               )
__mc3 = __mc2 + __mc1

__mc = block_matrix(GF(2),
                    [
                        [__mc2, __mc1, __mc1, __mc3],
                        [__mc3, __mc2, __mc1, __mc1],
                        [__mc1, __mc3, __mc2, __mc1],
                        [__mc1, __mc1, __mc3, __mc2],
                    ]
                    )

_MIX_COLUMNS_MATRIX = block_matrix(GF(2),
                                   [
                                       [__mc, matrix.zero(32), matrix.zero(32), matrix.zero(32)],
                                       [matrix.zero(32), __mc, matrix.zero(32), matrix.zero(32)],
                                       [matrix.zero(32), matrix.zero(32), __mc, matrix.zero(32)],
                                       [matrix.zero(32), matrix.zero(32), matrix.zero(32), __mc]
                                   ]
                                   )

_LINEAR_LAYER_MATRIX = _SHIFT_ROW_MATRIX * _MIX_COLUMNS_MATRIX

_LINEAR_LAYER_INVERSE_MATRIX = _LINEAR_LAYER_MATRIX.inverse()
__IDENTITY_128 = matrix.identity(GF(2), 128)
__ZERO_128 = matrix.zero(128)

SPN_MATRIX = block_matrix(GF(2),
                                  [
                                      [__IDENTITY_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128,
                                       __ZERO_128,
                                       __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128,
                                       __ZERO_128,
                                       __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128],
                                      [__ZERO_128, _LINEAR_LAYER_INVERSE_MATRIX, __IDENTITY_128, __ZERO_128, __ZERO_128,
                                       __ZERO_128, __ZERO_128,
                                       __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128,
                                       __ZERO_128,
                                       __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128],
                                      [__ZERO_128, __ZERO_128, __ZERO_128, _LINEAR_LAYER_INVERSE_MATRIX, __IDENTITY_128,
                                       __ZERO_128, __ZERO_128,
                                       __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128,
                                       __ZERO_128,
                                       __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128],
                                      [__ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128,
                                       _LINEAR_LAYER_INVERSE_MATRIX, __IDENTITY_128,
                                       __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128,
                                       __ZERO_128,
                                       __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128],
                                      [__ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128,
                                       __ZERO_128,
                                       _LINEAR_LAYER_INVERSE_MATRIX, __IDENTITY_128, __ZERO_128, __ZERO_128, __ZERO_128,
                                       __ZERO_128, __ZERO_128,
                                       __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128],
                                      [__ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128,
                                       __ZERO_128,
                                       __ZERO_128, __ZERO_128, _LINEAR_LAYER_INVERSE_MATRIX, __IDENTITY_128, __ZERO_128,
                                       __ZERO_128, __ZERO_128,
                                       __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128],
                                      [__ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128,
                                       __ZERO_128,
                                       __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, _LINEAR_LAYER_INVERSE_MATRIX,
                                       __IDENTITY_128, __ZERO_128,
                                       __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128],
                                      [__ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128,
                                       __ZERO_128,
                                       __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128,
                                       _LINEAR_LAYER_INVERSE_MATRIX,
                                       __IDENTITY_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128],
                                      [__ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128,
                                       __ZERO_128,
                                       __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128,
                                       __ZERO_128,
                                       __ZERO_128, _LINEAR_LAYER_INVERSE_MATRIX, __IDENTITY_128, __ZERO_128, __ZERO_128,
                                       __ZERO_128],
                                      [__ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128,
                                       __ZERO_128,
                                       __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128,
                                       __ZERO_128,
                                       __ZERO_128, __ZERO_128, __ZERO_128, _LINEAR_LAYER_INVERSE_MATRIX, __IDENTITY_128,
                                       __ZERO_128],
                                      [__ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128,
                                       __ZERO_128,
                                       __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128,
                                       __ZERO_128,
                                       __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128, __ZERO_128,
                                       _LINEAR_LAYER_INVERSE_MATRIX],
                                  ]
                                  )
