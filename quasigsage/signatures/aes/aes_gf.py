import logging

from hashlib import sha256

from sage.all import *
from sage.coding.linear_code import LinearCode
from sage.crypto.mq.rijndael_gf import RijndaelGF

from quasigsage.ciphers.aes import aes
from quasigsage.signatures.aes import constants_gf as aesconst
from quasigsage.signatures.helpers import generate_random_nonce_128_hex, generate_random_permutation


def key_generation():
    logging.info("Key generation AES bin started")
    # generate random master key of size 128 bits
    key = generate_random_nonce_128_hex()
    logging.debug("master key = {}".format(key))

    # run key schedule to get round keys
    round_keys = aes.key_expand(key, gf=True)
    logging.debug("round keys ({}) = {}".format(len(round_keys), round_keys))

    # convert round keys to constants and exclude first key (master key)
    constants = __key_schedule_to_constants(round_keys)

    # for convenient permuting we append constants as last row
    spn_matrix_with_constants = block_matrix(aesconst.AES_GF, [
        [aesconst.SPN_MATRIX],
        [constants]
    ])

    # convert system in loggingical format to actual -> (input of Sbox, output of Sbox) is one equation
    mrhs_system_with_constants = __get_mrhs_system(spn_matrix_with_constants)
    logging.debug("constants ({}) = {}".format(len(mrhs_system_with_constants.row(-1)),
                                             ''.join([format(i.integer_representation(), '02x') for i in
                                                      mrhs_system_with_constants.row(-1)])))

    # generate random permutation -> length is number of equations in system = number of sboxes times number of rounds
    random_permutation = generate_random_permutation(16 * 10, 16)
    logging.debug("random permutation = {}".format(random_permutation))

    # permute equations
    permuted_system_with_constants = __permute_system_equations(mrhs_system_with_constants, random_permutation)

    permuted_constants = permuted_system_with_constants.row(-1)
    logging.debug("permuted constants ({}) = {}".format(len(permuted_constants), ''.join(
        [format(i.integer_representation(), '02x') for i in permuted_constants])))

    permuted_system = permuted_system_with_constants[: -1, :]  # get rid of constants = last row
    logging.debug("permuted system ({}, {})".format(permuted_system.nrows(), permuted_system.ncols()))

    # compute transposed parity check matrix
    H_p_t = LinearCode(permuted_system).dual_code().generator_matrix().transpose()
    logging.debug("H_p_t ({}, {})".format(H_p_t.nrows(), H_p_t.ncols()))

    # compute syndrome
    q = permuted_constants * H_p_t
    logging.debug("q".format(len(q), q))

    # public key
    public_key = (H_p_t, q)

    # private key
    private_key = (key, random_permutation)

    logging.info("Key generation AES bin ended")
    return public_key, private_key


def sign(message, private_key):
    logging.info("Signing AES bin started")
    logging.debug("message = {}".format(message))
    logging.debug("private key = {}".format(private_key))
    key = private_key[0]
    permutation = private_key[1]

    # 1. generate nonce (128 bit number in hex)
    nonce = generate_random_nonce_128_hex()
    logging.debug("nonce = {}".format(nonce))

    # 2. generate hash of nonce + message and xor master key
    sha = sha256()
    p = nonce + message
    sha.update(p)
    p_hash = sha.hexdigest()[0:32]
    logging.debug("hash = {}".format(p_hash))
    p_hash_xor_key = format(int(p_hash, 16) ^ int(key, 16), '032x')
    logging.debug("hash XOR key = {}".format(p_hash_xor_key))

    # 3. encrypt p_hash and save S-box inputs
    _, u = aes.encrypt(p_hash_xor_key, key, gf=True)
    logging.debug("u ({}) = {}".format(len(u), u))

    # 4. permute S-box inputs
    w = permutation.action(u)
    logging.debug("w ({}) = {}".format(len(w), w))

    # 5. signature is pair of nonce and permuted S-box inputs
    signature = (nonce, w)
    logging.debug("signature = {}".format(signature))

    logging.info("Signing AES bin ended")
    return signature


def verify(message, signature, public_key):
    logging.info("Verifying AES bin started")
    logging.debug("message = {}".format(message))
    rijn = RijndaelGF(4, 4)

    # get private key values from tuple
    H_p_t = public_key[0]
    logging.debug("H_p_t ({}, {})".format(H_p_t.nrows(), H_p_t.ncols()))
    q = public_key[1]
    logging.debug("private key q ({}) = {}".format(len(q), [l.integer_representation() for l in q]))

    # get signature values from tuple
    nonce = signature[0]
    logging.debug("nonce = {}".format(nonce))
    w = signature[1]
    logging.debug("w ({}) = {}".format(len(w), w))

    # prepare h = (nonce|message)
    sha = sha256()
    p = nonce + message
    sha.update(p)
    p_hash = sha.hexdigest()[0:32]
    logging.debug("hash = {}".format(p_hash))

    # if h_sbox_in inputs are not same as w[0:16], then signature is not valid
    if [p_hash[i: i + 2] for i in range(0, len(p_hash), 2)] != w[0:16]:
        logging.debug("Signature is not valid. First set of sbox inputs do not match")
        return False

    # construct vector v of (w_i, S(w_i))
    v = ''
    for w_i in w:
        sbox_w = aes.sbox_value(int(w_i, 16))
        v += (str(w_i) + format(sbox_w, '02x'))

    # convert v in hex format to gf format
    v_poly = []
    for i in range(0, len(v), 32):
        v_poly += rijn._hex_to_GF(v[i: i + 32], matrix=False)

    # compute new syndrome
    v = vector(aesconst.AES_GF, v_poly)
    logging.debug("v ({}) = {}".format(len(v), [format(p.integer_representation(), '02x') for p in v]))
    q_wannabe = v * H_p_t
    logging.debug("new syndrome ({}) =  {}".format(len(q_wannabe), [p.integer_representation() for p in q_wannabe]))

    # compare syndromes
    if q != q_wannabe:
        logging.debug("Signature is not valid. Syndromes do not match")
        return False

    logging.debug("Signature is valid.")
    logging.info("Verifying AES bin ended")
    return True


# constants here reference to signature algorithm constants, see article
def __key_schedule_to_constants(round_keys):
    rijn = RijndaelGF(4, 4)

    result = []
    # Skip first key (original key)
    for i in range(1, len(round_keys)):
        result += [0 for _ in range(16)]
        result += vector(rijn._hex_to_GF(round_keys[i], matrix=False)) * aesconst._LINEAR_LAYER_INVERSE_MATRIX

    return matrix(aesconst.AES_GF, result)  # we need matrix, even tho it has just one row


def __get_mrhs_system(spn_matrix):
    permutation = Permutation([
        1, 17, 2, 18, 3, 19, 4, 20, 5, 21, 6, 22, 7, 23, 8, 24, 9, 25, 10, 26, 11, 27, 12, 28, 13,
        29, 14, 30, 15, 31, 16, 32, 33, 49, 34, 50, 35, 51, 36, 52, 37, 53, 38, 54, 39, 55, 40,
        56, 41, 57, 42, 58, 43, 59, 44, 60, 45, 61, 46, 62, 47, 63, 48, 64, 65, 81, 66, 82, 67,
        83, 68, 84, 69, 85, 70, 86, 71, 87, 72, 88, 73, 89, 74, 90, 75, 91, 76, 92, 77, 93, 78,
        94, 79, 95, 80, 96, 97, 113, 98, 114, 99, 115, 100, 116, 101, 117, 102, 118, 103, 119,
        104, 120, 105, 121, 106, 122, 107, 123, 108, 124, 109, 125, 110, 126, 111, 127, 112, 128,
        129, 145, 130, 146, 131, 147, 132, 148, 133, 149, 134, 150, 135, 151, 136, 152, 137, 153,
        138, 154, 139, 155, 140, 156, 141, 157, 142, 158, 143, 159, 144, 160, 161, 177, 162, 178,
        163, 179, 164, 180, 165, 181, 166, 182, 167, 183, 168, 184, 169, 185, 170, 186, 171, 187,
        172, 188, 173, 189, 174, 190, 175, 191, 176, 192, 193, 209, 194, 210, 195, 211, 196, 212,
        197, 213, 198, 214, 199, 215, 200, 216, 201, 217, 202, 218, 203, 219, 204, 220, 205, 221,
        206, 222, 207, 223, 208, 224, 225, 241, 226, 242, 227, 243, 228, 244, 229, 245, 230, 246,
        231, 247, 232, 248, 233, 249, 234, 250, 235, 251, 236, 252, 237, 253, 238, 254, 239, 255,
        240, 256, 257, 273, 258, 274, 259, 275, 260, 276, 261, 277, 262, 278, 263, 279, 264, 280,
        265, 281, 266, 282, 267, 283, 268, 284, 269, 285, 270, 286, 271, 287, 272, 288, 289, 305,
        290, 306, 291, 307, 292, 308, 293, 309, 294, 310, 295, 311, 296, 312, 297, 313, 298, 314,
        299, 315, 300, 316, 301, 317, 302, 318, 303, 319, 304, 320
    ])
    equations = []

    for eq in range(320):
        equations.append(spn_matrix[:, eq])

    actual_system_equations = permutation.action(equations)

    result = block_matrix(aesconst.AES_GF, [actual_system_equations])
    return result


def __permute_system_equations(system, permutation):
    equations = []

    for eq in range(160):
        equations.append(system[:, eq * 2: eq * 2 + 2])

    permuted_equations = permutation.action(equations)

    result = block_matrix(aesconst.AES_GF, [permuted_equations])
    return result
