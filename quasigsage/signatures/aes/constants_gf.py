from sage.matrix.all import matrix
from sage.matrix.special import block_matrix
from sage.rings.finite_rings.finite_field_constructor import FiniteField
from sage.rings.finite_rings.integer_mod_ring import Integers
from sage.rings.polynomial.polynomial_ring import polygen

__x = polygen(Integers(2))
__aes_modulus = __x ** 8 + __x ** 4 + __x ** 3 + __x + 1

AES_GF = FiniteField(2 ** 8, 'x', modulus=__aes_modulus)

_SHIFT_ROW_MATRIX = matrix(AES_GF,
                           [
                               [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                               [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
                               [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
                               [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
                               [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                               [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                               [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
                               [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
                               [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
                               [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                               [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                               [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
                               [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
                               [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
                               [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                               [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                           ])

_MIX_COLUMNS_MATRIX = matrix(AES_GF,
                             [
                                 [__x, 1, 1, __x + 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                 [__x + 1, __x, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                 [1, __x + 1, __x, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                 [1, 1, __x + 1, __x, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                                 [0, 0, 0, 0, __x, 1, 1, __x + 1, 0, 0, 0, 0, 0, 0, 0, 0],
                                 [0, 0, 0, 0, __x + 1, __x, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0],
                                 [0, 0, 0, 0, 1, __x + 1, __x, 1, 0, 0, 0, 0, 0, 0, 0, 0],
                                 [0, 0, 0, 0, 1, 1, __x + 1, __x, 0, 0, 0, 0, 0, 0, 0, 0],
                                 [0, 0, 0, 0, 0, 0, 0, 0, __x, 1, 1, __x + 1, 0, 0, 0, 0],
                                 [0, 0, 0, 0, 0, 0, 0, 0, __x + 1, __x, 1, 1, 0, 0, 0, 0],
                                 [0, 0, 0, 0, 0, 0, 0, 0, 1, __x + 1, __x, 1, 0, 0, 0, 0],
                                 [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, __x + 1, __x, 0, 0, 0, 0],
                                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, __x, 1, 1, __x + 1],
                                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, __x + 1, __x, 1, 1],
                                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, __x + 1, __x, 1],
                                 [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, __x + 1, __x]
                             ]
                             )

_LINEAR_LAYER_MATRIX = _SHIFT_ROW_MATRIX * _MIX_COLUMNS_MATRIX

_LINEAR_LAYER_INVERSE_MATRIX = _LINEAR_LAYER_MATRIX.inverse()
__IDENTITY_16 = matrix.identity(AES_GF, 16)
__ZERO_16 = matrix.zero(AES_GF, 16)

# beauty
SPN_MATRIX = block_matrix(AES_GF,
                          [
                              [__IDENTITY_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16,
                               __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16,
                               __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16,
                               __ZERO_16],
                              [__ZERO_16, _LINEAR_LAYER_INVERSE_MATRIX, __IDENTITY_16, __ZERO_16, __ZERO_16, __ZERO_16,
                               __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16,
                               __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16,
                               __ZERO_16, __ZERO_16],
                              [__ZERO_16, __ZERO_16, __ZERO_16, _LINEAR_LAYER_INVERSE_MATRIX, __IDENTITY_16, __ZERO_16,
                               __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16,
                               __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16,
                               __ZERO_16, __ZERO_16],
                              [__ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, _LINEAR_LAYER_INVERSE_MATRIX,
                               __IDENTITY_16, __ZERO_16, __ZERO_16, __ZERO_16,
                               __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16,
                               __ZERO_16, __ZERO_16],
                              [__ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16,
                               _LINEAR_LAYER_INVERSE_MATRIX, __IDENTITY_16, __ZERO_16,
                               __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16,
                               __ZERO_16, __ZERO_16],
                              [__ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16,
                               __ZERO_16, _LINEAR_LAYER_INVERSE_MATRIX,
                               __IDENTITY_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16,
                               __ZERO_16, __ZERO_16, __ZERO_16],
                              [__ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16,
                               __ZERO_16, __ZERO_16, __ZERO_16,
                               _LINEAR_LAYER_INVERSE_MATRIX, __IDENTITY_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16,
                               __ZERO_16, __ZERO_16, __ZERO_16],
                              [__ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16,
                               __ZERO_16, __ZERO_16, __ZERO_16,
                               __ZERO_16, __ZERO_16, _LINEAR_LAYER_INVERSE_MATRIX, __IDENTITY_16, __ZERO_16, __ZERO_16,
                               __ZERO_16, __ZERO_16, __ZERO_16],
                              [__ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16,
                               __ZERO_16, __ZERO_16, __ZERO_16,
                               __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, _LINEAR_LAYER_INVERSE_MATRIX, __IDENTITY_16,
                               __ZERO_16, __ZERO_16, __ZERO_16],
                              [__ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16,
                               __ZERO_16, __ZERO_16, __ZERO_16,
                               __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16,
                               _LINEAR_LAYER_INVERSE_MATRIX, __IDENTITY_16, __ZERO_16],
                              [__ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16,
                               __ZERO_16, __ZERO_16, __ZERO_16,
                               __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16, __ZERO_16,
                               _LINEAR_LAYER_INVERSE_MATRIX],
                          ])
