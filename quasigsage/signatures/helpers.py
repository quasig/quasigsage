from random import shuffle

from sage.all import *
from sage.combinat.permutation import Permutation
from sage.rings.finite_rings.all import GF
from sage.rings.all import Integer


def generate_random_nonce_128_hex():
    galois = GF(2 ** 128)
    random_hex = Integer(galois.random_element().integer_representation()).hex()
    while len(random_hex) is not 32:
        random_hex = Integer(galois.random_element().integer_representation()).hex()
    return random_hex


def generate_random_number_hex(bits):
    galois = GF(2 ** bits, 'x')
    rnd = galois.random_element().polynomial().list()
    while len(rnd) != bits or rnd[0] != 1 or rnd[bits - 1] != 1:
        rnd = galois.random_element().polynomial().list()
    # print len(rnd), rnd
    bins = ''.join([str(_) for _ in rnd])
    return format(int(bins, 2), 'x')


def generate_random_permutation(length, fix=0):
    assert length > 0
    assert 0 <= fix <= length

    st = []
    if fix > 0:
        st = range(1, fix + 1)

    interval = range(fix + 1, length + 1)
    shuffle(interval)

    return Permutation(st + interval)
