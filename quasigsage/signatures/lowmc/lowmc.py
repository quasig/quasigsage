import logging

from sage.all import *
from sage.coding.linear_code import LinearCode

from quasigsage.ciphers.lowmc import lowmc
from quasigsage.ciphers.lowmc.lowmc import LowMCInstance, key_schedule
from quasigsage.helpers import finite_field_to_bin, bin_to_finite_field, hex_to_bin, hex_to_finite_field, \
    finite_field_to_hex
from quasigsage.signatures.helpers import generate_random_number_hex, generate_random_permutation

from hashlib import sha256

logging.getLogger().setLevel(logging.DEBUG)


def key_generation(instance):
    key_size = instance.key_size
    rounds = instance.rounds
    number_of_sboxes = instance.number_of_sboxes

    # generate a random key
    key = generate_random_number_hex(key_size)

    # create mrhs system matrix
    spn_system = create_spn_system(key, instance)
    system = create_mrhs_system(spn_system, instance)

    # generate a random permutation and permute system
    permutation = generate_random_permutation(number_of_sboxes * rounds, number_of_sboxes)
    permuted_system = __permute_system(system, permutation, instance)

    # get rid of last row = constants from system
    constants = permuted_system.row(-1)
    permuted_system = permuted_system[: -1, :]

    # compute systematic parity check matrix
    H_p_t = LinearCode(permuted_system).dual_code().generator_matrix().transpose()

    # compute syndrome
    q = constants * H_p_t

    # public key with instance as additional parameter
    public_key = (H_p_t, q, instance)

    # private key
    private_key = (key, permutation)

    return public_key, private_key


def create_spn_system(key, instance):
    block_size = instance.block_size / instance.field.degree()
    sbox_size = instance.sbox_size / instance.field.degree()

    # compute lowmc key schedule
    round_keys = key_schedule(key, instance)

    # compute algorithm constants -> round_constants + round_key
    constants = __compute_constants(round_keys, instance.round_constants)

    U_rounds = [__construct_u(round, instance) for round in range(1, instance.rounds + 1)]

    # compute initial R0 matrix
    R0 = block_matrix(instance.field, [
        [identity_matrix(instance.field, block_size)],
        [zero_matrix(instance.field, sbox_size * instance.number_of_sboxes * instance.rounds, block_size)],
        [zero_matrix(instance.field, 1, block_size)]
        # [matrix(instance.field, 1, block_size, constants[1])]
    ])

    # poly_ring = PolynomialRing(instance.field, 's', R0.nrows())
    # variables = vector(poly_ring.gens())

    # V0 and T0
    v = R0[:, :sbox_size * instance.number_of_sboxes]
    tmp = R0[:, sbox_size * instance.number_of_sboxes:]

    U_1 = matrix(U_rounds[0])
    # U_1[-1] = zero_vector(sbox_size * instance.number_of_sboxes)
    system = block_matrix(instance.field, [
        [v, U_1]
    ])

    for round in range(2, instance.rounds + 1):
        # pick a part of state -> identity on round part
        # U_round = __construct_u(round, instance)

        # split constants to two parts
        # constants_prev_round_first = constants[round - 1][:sbox_size * instance.number_of_sboxes]
        # constants_prev_round_second = constants[round - 1][sbox_size * instance.number_of_sboxes:]

        # put first constants as last row in U
        # U_prev_round = block_matrix(instance.field, [
        #     [U_rounds[round - 2]],
        #     [matrix(instance.field, 1, sbox_size * instance.number_of_sboxes, constants_prev_round_first)]
        # ])
        U_prev_round = matrix(U_rounds[round - 2])

        # add second "half" of constants to last row of tmp
        # tmp[-1] += constants_prev_round_second

        # compute new R -> (U|T)*L
        R_round = block_matrix([
            [U_prev_round, tmp]
        ]) * instance.linear_matrices[round - 2]

        R_round[-1] += constants[round - 1]  # todo sprav rovnake indexy aby sa dalo lahsie kontrolovat

        # pick new v and tmp
        v = R_round[:, :sbox_size * instance.number_of_sboxes]
        tmp = R_round[:, sbox_size * instance.number_of_sboxes:]

        # constants_round_first = constants[round][:sbox_size * instance.number_of_sboxes]
        # constants_round_second = constants[round][sbox_size * instance.number_of_sboxes:]
        # U_round = block_matrix(instance.field, [
        #     [U_rounds[round - 1]],
        #     [matrix(instance.field, 1, sbox_size * instance.number_of_sboxes, constants_round_first)]
        # ])
        U_round = matrix(U_rounds[round - 1])
        # U_round[-1] = zero_vector(sbox_size * instance.number_of_sboxes)
        # tmp[-1] += constants_round_second

        # put v in system
        system = block_matrix(instance.field, [
            [system, v, U_round]
        ])

    # print system
    # matrix_plot(system).save_image("system.png")
    return system


def __compute_constants(round_keys, round_constants):
    # constants = [round_keys[0]]
    constants = [zero_vector(len(round_keys[0]))]
    return constants + [round_keys[i + 1] + round_constants[i] for i in range(len(round_constants))]
    # return round_constants


def __construct_u(round, instance):
    block_size = instance.block_size / instance.field.degree()
    sbox_size = instance.sbox_size / instance.field.degree()

    identity = identity_matrix(instance.field, sbox_size * instance.number_of_sboxes)
    zero = zero_matrix(instance.field, sbox_size * instance.number_of_sboxes)
    U_round = zero_matrix(instance.field, block_size, sbox_size * instance.number_of_sboxes)
    for i in range(1, instance.rounds + 1):
        U_round = block_matrix(instance.field, [
            [U_round],
            [identity if round == i else zero]
        ])
    U_round = block_matrix(instance.field, [
        [U_round],
        [zero_matrix(instance.field, 1, sbox_size * instance.number_of_sboxes)]
    ])
    return U_round


def create_mrhs_system(spn_system, instance):
    permutation = []
    in_out_size = instance.sbox_size / instance.field.degree()

    cnt = 1
    for i in range(instance.number_of_sboxes * instance.rounds * 2):
        permutation.append(cnt)
        if i % 2 == 0:
            cnt += instance.number_of_sboxes
        else:
            cnt -= instance.number_of_sboxes - 1
            if cnt in permutation:
                cnt += instance.number_of_sboxes
    permutation = Permutation(permutation)

    equations = []
    for eq in range(instance.number_of_sboxes * instance.rounds * 2):
        equations.append(spn_system[:, eq * in_out_size: eq * in_out_size + in_out_size])

    permuted_equations = permutation.action(equations)

    return block_matrix(instance.field, [permuted_equations])


def __permute_system(system, permutation, instance):
    eq_size = (instance.sbox_size / instance.field.degree()) * 2  # ins and outs
    equations = []

    for eq in range(system.ncols() / eq_size):
        equations.append(system[:, eq * eq_size: eq * eq_size + eq_size])
    permuted_equations = permutation.action(equations)

    return block_matrix(instance.field, [permuted_equations])


def sign(message, private_key, instance, algo='out'):
    key = private_key[0]
    permutation = private_key[1]

    # 1. generate nonce (128 bit number in hex)
    nonce = generate_random_number_hex(128)

    # 2. generate hash of nonce + message and xor master key
    sha = sha256()
    p = nonce + message
    sha.update(p)
    p_hash = sha.hexdigest()
    p_hash_bin = hex_to_bin(p_hash)
    p_hash = format(int(p_hash_bin[:instance.block_size], 2), 'x')

    # 3. encrypt p_hash and save S-box outputs or inputs depending on algo parameter
    _, u_in, u_out = lowmc.encrypt(p_hash, key, instance)

    # 4. permute S-box inputs
    if algo == 'out':
        w = permutation.action(u_out)
    elif algo == 'in':
        w = permutation.action(u_in)
    else:
        raise Exception("Algo must be either 'in' or 'out'")
    # map elements to binary
    w = map(lambda x: finite_field_to_bin(x, instance.field), w)

    # 5. signature is pair of nonce and permuted S-box inputs
    signature = (nonce, w)
    return signature


def verify(message, signature, public_key, algo='out'):
    # get private key values from tuple
    H_p_t = public_key[0]
    q = public_key[1]
    instance = public_key[2]

    # get signature values from tuple
    nonce = signature[0]
    w = signature[1]

    # prepare h = (nonce|message)
    sha = sha256()
    p = nonce + message
    sha.update(p)
    p_hash = sha.hexdigest()
    p_hash_bin = hex_to_bin(p_hash)
    p_hash = format(int(p_hash_bin[:instance.block_size], 2), 'x')
    state = hex_to_finite_field(p_hash, instance.field, fill_bits=instance.block_size)
    encrypted = finite_field_to_hex(state, instance.field, fill_bits=instance.block_size)

    p_hash_bin = hex_to_bin(encrypted)[:instance.number_of_sboxes * instance.sbox_size]

    # if h_sbox_in inputs are not same as w[0:16], then signature is not valid
    if algo == 'out':
        w_to_check = [''.join([str(i) for i in instance.inverse_sbox([int(k) for k in w_i])]) for w_i in w[0:instance.number_of_sboxes]]
    elif algo == 'in':
        w_to_check = w[0:instance.number_of_sboxes]
    else:
        raise Exception("Algo must be either 'in' or 'out'")

    w_to_check = ''.join(w_to_check)
    if w_to_check != p_hash_bin:
        print("Signature is not valid. First set of sbox inputs do not match")
        return False

    # construct vector v of (w_i, S(w_i))
    v = ''
    for w_i in w:
        if algo == 'out':
            sbox_in = instance.inverse_sbox(w_i)
            v += (''.join([str(i) for i in sbox_in]) + w_i)
        elif algo == 'in':
            sbox_out = instance.sbox(w_i)
            v += (w_i + ''.join([str(i) for i in sbox_out]))
        else:
            raise Exception("Algo must be either 'in' or 'out'")

    # convert v in hex format to gf format
    v_field = bin_to_finite_field(v, instance.field)

    # compute new syndrome
    q_wannabe = v_field * H_p_t

    # compare syndromes
    if q != q_wannabe:
        print("Signature is not valid. Syndromes do not match")
        return False

    return True
