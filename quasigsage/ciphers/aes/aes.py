from sage.crypto.mq.rijndael_gf import RijndaelGF

import logging

from quasigsage.ciphers.aes.constants import AES_SBOX


def sbox_value(val):
    return AES_SBOX(val)


def key_expand(key, gf=False):
    assert len(key) == 32

    aes = RijndaelGF(4, 4)
    key_state = aes._hex_to_GF(key)
    key_schedule = aes.expand_key(key_state)

    if gf:
        round_keys = [aes._GF_to_hex(i) for i in key_schedule]
    else:
        round_keys = [aes._GF_to_bin(i) for i in key_schedule]

    return round_keys


def encrypt(plain_text, key, gf=False):
    s_box_inputs = []
    logging.debug("Plaintext = {}".format(plain_text))

    aes = RijndaelGF(4, 4)
    state = aes._hex_to_GF(plain_text)
    key_state = aes._hex_to_GF(key)
    key_schedule = aes.expand_key(key_state)

    # initial round 0
    # AddRoundKey
    state = aes.add_round_key(state, key_schedule[0])

    for round in range(1, 10):
        # inputs of sbox
        s_box_inputs.append(aes._GF_to_hex(state) if gf else aes._GF_to_bin(state))
        # SubBytes
        logging.debug(
            "Round {}, SubBytes input = {}".format(round, aes._GF_to_hex(state) if gf else aes._GF_to_bin(state))
        )
        state = aes.sub_bytes(state)
        # ShiftRows

        logging.debug(
            "Round {}, ShiftRows input = {}".format(round, aes._GF_to_hex(state) if gf else aes._GF_to_bin(state))
        )
        state = aes.shift_rows(state)
        # MixColumns

        logging.debug("Round {}, MixColumns input = {}".format(round,
                                                               aes._GF_to_hex(state) if gf else aes._GF_to_bin(state))
                      )
        state = aes.mix_columns(state)
        # AddRoundKey

        logging.debug("Round {}, AddRoundKey input = {}".format(round,
                                                                aes._GF_to_hex(state) if gf else aes._GF_to_bin(state))
                      )

        logging.debug("Round {}, RoundKey = {}".format(round,
                                                       aes._GF_to_hex(key_schedule[round]) if gf else aes._GF_to_bin(
                                                           key_schedule[round]))
                      )
        state = aes.add_round_key(state, key_schedule[round])

    s_box_inputs.append(aes._GF_to_hex(state) if gf else aes._GF_to_bin(state))

    logging.debug("Round {}, SubBytes input = {}".format(10, aes._GF_to_hex(state) if gf else aes._GF_to_bin(state)))
    state = aes.sub_bytes(state)

    logging.debug("Round {}, ShiftRows input = {}".format(10, aes._GF_to_hex(state) if gf else aes._GF_to_bin(state)))
    state = aes.shift_rows(state)

    logging.debug("Round {}, AddRoundKey input = {}".format(10, aes._GF_to_hex(state) if gf else aes._GF_to_bin(state)))
    state = aes.add_round_key(state, key_schedule[10])

    logging.debug("After encryption = {}".format(aes._GF_to_hex(state)))

    # process sbox inputs - take every byte
    result = []
    if gf:
        for sin in s_box_inputs:
            for sbox in range(16):
                result.append(sin[sbox * 2: sbox * 2 + 2])
    else:
        for sin in s_box_inputs:
            for sbox in range(16):
                result.append(sin[sbox * 8: sbox * 8 + 8])

    return aes._GF_to_hex(state), result
