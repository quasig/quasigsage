from sage.all import *

from quasigsage.ciphers.lowmc.matrix_generator import generate_linear_matrices, generate_key_matrices, \
    generate_round_constants
from quasigsage.helpers import bin_to_finite_field, hex_to_finite_field, finite_field_to_bin, \
    finite_field_to_hex


class LowMCInstance:

    def __init__(self, block_size, key_size, rounds, sbox, number_of_sboxes, field=GF(2)):
        self.block_size = block_size
        self.key_size = key_size
        self.rounds = rounds

        # sbox and identity
        assert sbox.input_size() == sbox.output_size()
        if field.degree() > 1:
            assert sbox.input_size() % field.degree() == 0
        self.sbox = sbox
        self.inverse_sbox = sbox.inverse()
        self.number_of_sboxes = number_of_sboxes
        self.sbox_size = sbox.input_size()  # same as sbox.output_size()
        self.identity_size = block_size - self.sbox_size * number_of_sboxes
        assert self.identity_size <= block_size or self.identity_size >= 0
        assert self.identity_size % field.degree() == 0

        # matrices and constants
        self.field = field
        assert block_size % field.degree() == 0
        assert key_size % field.degree() == 0
        self.linear_matrices, self.inverse_linear_matrices = generate_linear_matrices(block_size // field.degree(),
                                                                                      rounds, field)
        self.key_matrices = generate_key_matrices(block_size // field.degree(), key_size // field.degree(), rounds,
                                                  field)
        self.round_constants = generate_round_constants(block_size // field.degree(), rounds, field)


def _substitution(state, instance):
    sbox_inputs, sbox_outputs = [], []
    res = ''

    s = finite_field_to_bin(state, instance.field, fill=instance.block_size)
    for n in range(instance.number_of_sboxes):
        sbox_input = [int(i) for i in s[n * instance.sbox_size: instance.sbox_size * (n + 1)]]
        sbox_output = instance.sbox(sbox_input)
        sbox_input_str = ''.join([str(i) for i in sbox_input])
        sbox_output_str = ''.join([str(i) for i in sbox_output])

        res += sbox_output_str

        sbox_inputs.append(bin_to_finite_field(sbox_input_str, instance.field))
        sbox_outputs.append(bin_to_finite_field(sbox_output_str, instance.field))

    res += s[instance.sbox_size * instance.number_of_sboxes:]
    state = bin_to_finite_field(res, instance.field, fill=instance.block_size)
    return state, sbox_inputs, sbox_outputs


def _inverse_substitution(state, instance):
    res = ''

    s = finite_field_to_bin(state, instance.field, fill=instance.block_size)
    for n in range(instance.number_of_sboxes):
        res += ''.join(str(i) for i in instance.inverse_sbox(s[n * instance.sbox_size: instance.sbox_size * (n + 1)]))

    res += s[instance.sbox_size * instance.number_of_sboxes:]
    state = bin_to_finite_field(res, instance.field, fill=instance.block_size)
    return state


def key_schedule(key, instance):
    key_in_field = hex_to_finite_field(key, instance.field, fill_bits=instance.key_size)
    assert len(key_in_field) == instance.key_size // instance.field.degree()
    return [key_mat * key_in_field for key_mat in instance.key_matrices]


def encrypt(plain_text, key, instance):
    sbox_inputs, sbox_outputs = [], []

    state = hex_to_finite_field(plain_text, instance.field, fill_bits=instance.block_size)
    assert len(state) == instance.block_size // instance.field.degree()

    round_keys = key_schedule(key, instance)

    # state = state + round_keys[0]
    for round in range(1, instance.rounds + 1):
        state, sbox_in, sbox_out = _substitution(state, instance)
        sbox_inputs += sbox_in
        sbox_outputs += sbox_out
        state = state * instance.linear_matrices[round - 1]
        state = state + instance.round_constants[round - 1]
        state = state + round_keys[round]

    encrypted = finite_field_to_hex(state, instance.field, fill_bits=instance.block_size)
    return encrypted, sbox_inputs, sbox_outputs


def decrypt(cipher_text, key, instance):
    round_keys = key_schedule(key, instance)

    state = hex_to_finite_field(cipher_text, instance.field, fill_bits=instance.block_size)
    for round in range(instance.rounds, 0, -1):
        state = state + round_keys[round]
        state = state + instance.round_constants[round - 1]
        state = state * instance.inverse_linear_matrices[round - 1]
        state = _inverse_substitution(state, instance)
    # state = state + round_keys[0]

    return finite_field_to_hex(state, instance.field, fill_bits=instance.block_size)
