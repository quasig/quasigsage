from sage.all import *


def generate_round_constants(block_size, rounds, field=GF(2)):
    return [random_vector(field, block_size) for _ in range(rounds)]


def generate_key_matrices(block_size, key_size, rounds, field=GF(2)):
    key_matrices = []

    for _ in range(rounds + 1):
        mat = random_matrix(field, block_size, key_size)
        while mat.rank() < min(block_size, key_size):
            mat = random_matrix(field, block_size, key_size)
        key_matrices.append(mat)

    return key_matrices


def generate_linear_matrices(block_size, rounds, field=GF(2)):
    linear_matrices = []
    inverse_linear_matrices = []

    for _ in range(rounds):
        mat = random_matrix(field, block_size, block_size)
        while mat.rank() != block_size:
            mat = random_matrix(field, block_size, block_size)
        linear_matrices.append(mat)
        inverse_linear_matrices.append(mat.inverse())

    return linear_matrices, inverse_linear_matrices
