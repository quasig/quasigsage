import quasigsage.signatures.aes.aes_bin as aes_bin
import quasigsage.signatures.aes.aes_gf as aes_gf
import quasigsage.signatures.lowmc.lowmc as lowmc
import quasigsage.ciphers.aes.constants as aes_constants
import quasigsage.ciphers.lowmc.constants as lowmc_constants
import quasigsage.ciphers.lowmc.lowmc as lowmc_cipher

# aes bin signature
key_generation_aes_bin = aes_bin.key_generation
sign_aes_bin = aes_bin.sign
verify_aes_bin = aes_bin.verify

# aes gf signature
key_generation_aes_gf = aes_gf.key_generation
sign_aes_gf = aes_gf.sign
verify_aes_gf = aes_gf.verify

# aes constants
AES_SBOX = aes_constants.AES_SBOX

# lowmc signature
key_generation_lowmc = lowmc.key_generation
sign_lowmc = lowmc.sign
verify_lowmc = lowmc.verify
create_mrhs_system_lowmc = lowmc.create_mrhs_system
create_spn_system_lowmc = lowmc.create_spn_system

#lowmc cipher
LowMCInstance = lowmc_cipher.LowMCInstance

# lowmc constants
LOWMC_SBOX = lowmc_constants.LOWMC_SBOX

__all__ = ['key_generation_aes_bin', 'sign_aes_bin', 'verify_aes_bin', 'key_generation_aes_gf', 'sign_aes_gf',
           'verify_aes_gf', 'AES_SBOX', 'key_generation_lowmc', 'sign_lowmc', 'verify_lowmc', 'create_mrhs_system_lowmc',
           'create_spn_system_lowmc', 'LowMCInstance', 'LOWMC_SBOX']
