# Post-quantum digital signature scheme

## Dependencies
All you need is to have SageMath installed. The version we used is 8.9. We have not tested
the algorithms with different version of Sage, but we expect it to be working with anything below 
version 9.0 (this version of sage uses 3.7 python instead of 2.7 used in <9.0 versions)

## Importing
1. First of all, you need to clone this repository
1. Open up Sage console in the root directory of cloned repository
1. And simply import our package with loading root __init.py file: 
    **load('quasigsage/\_\_init\_\_.py')**
1. For all imported functions see API section


## API
We are exporting these functions:
- AES binary
    - public_key, private_key = key_generation_aes_bin()
    - signature = sign_aes_bin(message, private_key)
    - verify =  verify_aes_bin(message, signature, public_key)
- AES gf256
    - public_key, private_key = key_generation_aes_gf()
    - signature = sign_aes_gf(message, private_key)
    - verify =  verify_aes_gf(message, signature, public_key)
- LowMC
    - public_key, private_key = key_generation_lowmc(instance)
    - signature = sign_lowmc(message, private_key, instance)
    - verify =  verify_lowmc(message, signature, public_key)
    - spn_system =  create_spn_system_lowmc(key, instance)
    - mrhs_system = create_mrhs_system_lowmc(spn_system, instance)

Exported constants:
- AES
    - AES_SBOX
- LowMC
    - LOWMC_SBOX
    
Classes:
- LowMC
    - LowMCInstance(block_size, key_size, rounds, sbox, number_of_sboxes, field=GF(2))