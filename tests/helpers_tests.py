import unittest
from sage.all import *

from quasigsage.helpers import hex_to_finite_field, finite_field_to_hex, bin_to_finite_field, finite_field_to_bin


class HelpersTest(unittest.TestCase):


    def test_conversion_small_hex_with_fill_to_finite_field_and_back(self):
        hx = "b1c1"
        field = FiniteField(2 ** 8, 'x')
        rpr = hex_to_finite_field(hx, field, fill_bits=32)
        back = finite_field_to_hex(rpr, field, fill_bits=32)
        self.assertEqual(hx, back)


    def test_conversion_bin_with_fill_to_gf2(self):
        bn = "10010101100101"
        field = GF(2)
        rpr = bin_to_finite_field(bn, field, fill=48)
        back = finite_field_to_bin(rpr, field, fill=48)
        self.assertEqual(bn.zfill(48), back)

    def test_conversion_bin_to_gf2(self):
        bn = "100101011001010110010101100101011001010110010101"
        field = GF(2)
        rpr = bin_to_finite_field(bn, field)
        back = finite_field_to_bin(rpr, field)
        self.assertEqual(bn, back)

    def test_conversion_bin_to_gf2_2(self):
        bn = "100"
        field = GF(2)
        rpr = bin_to_finite_field(bn, field)
        back = finite_field_to_bin(rpr, field)
        self.assertEqual(bn, back)

    def test_conversion_bin_to_finite_field_and_back(self):
        bn = "100101011001010110010101100101011001010110010101"
        field = FiniteField(2 ** 8, 'x')
        rpr = bin_to_finite_field(bn, field)
        back = finite_field_to_bin(rpr, field)
        self.assertEqual(bn, back)

    def test_conversion_small_hex_to_finite_field_and_back(self):
        hx = "b1c1"
        field = FiniteField(2 ** 8, 'x')
        rpr = hex_to_finite_field(hx, field)
        back = finite_field_to_hex(rpr, field)
        self.assertEqual(hx, back)

    def test_conversion_big_hex_to_finite_field_and_back(self):
        hx = "b1c1b1c1b1c1b1c1b1c1b1c1b1c1"
        field = FiniteField(2 ** 8, 'x')
        rpr = hex_to_finite_field(hx, field)
        back = finite_field_to_hex(rpr, field)
        self.assertEqual(hx, back)


if __name__ == '__main__':
    unittest.main()
