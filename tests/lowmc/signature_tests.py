from sage.all import *

import unittest

from quasigsage.signatures.lowmc import lowmc
from quasigsage.ciphers.lowmc.lowmc import LowMCInstance
from quasigsage.ciphers.aes.constants import AES_SBOX
from quasigsage.ciphers.lowmc.constants import LOWMC_SBOX


class LowMcSignatureTest(unittest.TestCase):

    def setUp(self):
        self.message = "Ahoj ako sa mas"
        self.different_message = "Ahoj ako sa mas 2"
        self.block_size = 256
        self.key_size = 80
        self.rounds = 12
        self.sbox1 = LOWMC_SBOX
        self.sbox2 = AES_SBOX
        self.number_of_sboxes = 16
        self.instance1 = LowMCInstance(256, 80, 12, LOWMC_SBOX, 16)
        self.instance2 = LowMCInstance(256, 80, 12, AES_SBOX, 16, field=GF(2 ** 4))


    def test_lowmc_gf2_valid_signature(self):
        public_key, private_key = lowmc.key_generation(self.instance1)
        signature = lowmc.sign(self.message, private_key, public_key[2])
        valid = lowmc.verify(self.message, signature, public_key)
        self.assertEqual(True, valid)

    def test_lowmc_gf2_invalid_signature_of_different_message(self):
        public_key, private_key = lowmc.key_generation(self.instance1)
        signature = lowmc.sign(self.message, private_key, public_key[2])
        valid = lowmc.verify(self.different_message, signature, public_key)
        self.assertEqual(False, valid)

    def test_lowmc_gf16_valid_signature(self):
        public_key, private_key = lowmc.key_generation(self.instance2)
        signature = lowmc.sign(self.message, private_key, public_key[2])
        valid = lowmc.verify(self.message, signature, public_key)
        print 1, valid
        signature = lowmc.sign(self.message, private_key, public_key[2])
        valid = lowmc.verify(self.message, signature, public_key)
        print 2, valid

        signature = lowmc.sign(self.message, private_key, public_key[2])
        valid = lowmc.verify(self.message, signature, public_key)
        print 3, valid

        signature = lowmc.sign(self.message, private_key, public_key[2])
        valid = lowmc.verify(self.message, signature, public_key)
        print 4, valid

        self.assertEqual(True, valid)

    def test_lowmc_gf16_invalid_signature_of_different_message(self):
        public_key, private_key = lowmc.key_generation(self.instance2)
        signature = lowmc.sign(self.message, private_key, public_key[2])
        valid = lowmc.verify(self.different_message, signature, public_key)
        self.assertEqual(False, valid)


if __name__ == '__main__':
    unittest.main()
