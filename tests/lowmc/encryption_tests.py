import unittest

from sage.all import *

from quasigsage.ciphers.aes.constants import AES_SBOX
from quasigsage.ciphers.lowmc.constants import LOWMC_SBOX
from quasigsage.ciphers.lowmc.lowmc import LowMCInstance, encrypt, decrypt


class LowMcEncryptionTest(unittest.TestCase):

    def test_encrypt_decrypt_equality_in_gf256_with_aes_sbox(self):
        field = FiniteField(2 ** 8, 'x')
        message = "f3a4a36ff30fc67005acf8dbf9a0fc9313a4a36ff30fc67005acf8dbf9a0fc93"
        key = "f665464fa6373153079f"
        instance = LowMCInstance(256, 80, 12, AES_SBOX, 16, field=field)
        enc, _, _ = encrypt(message, key, instance)
        dec = decrypt(enc, key, instance)
        self.assertEqual(message, dec)

    def test_encrypt_decrypt_equality_in_gf256_with_zfill_with_aes_sbox(self):
        field = FiniteField(2 ** 8, 'x')
        message = "f3a4a36ff30fc67005acf8dbf9a0"
        key = "f665464fa637"
        instance = LowMCInstance(256, 80, 12, AES_SBOX, 16, field=field)
        enc, _, _ = encrypt(message, key, instance)
        dec = decrypt(enc, key, instance)
        self.assertEqual(message, dec)

    def test_encrypt_decrypt_equality_in_gf2(self):
        field = GF(2)
        message = "f3a4a36ff30fc67005acf8dbf9a0fc9313a4a36ff30fc67005acf8dbf9a0fc93"
        key = "f665464fa6373153079f"
        instance = LowMCInstance(256, 80, 12, LOWMC_SBOX, 49, field=field)
        enc, _, _ = encrypt(message, key, instance)
        dec = decrypt(enc, key, instance)
        self.assertEqual(message, dec)

    def test_encrypt_decrypt_equality_in_gf2_with_zfill(self):
        field = GF(2)
        message = "f3a4a36ff30fc67005acf8dbf9a0"
        key = "f665464fa637"
        instance = LowMCInstance(256, 80, 12, LOWMC_SBOX, 49, field=field)
        enc, _, _ = encrypt(message, key, instance)
        dec = decrypt(enc, key, instance)
        self.assertEqual(message, dec)

    def test_encrypt_decrypt_equality_in_gf16_with_aes_sbox(self):
        field = GF(2 ** 4)
        message = "f3a4a36ff30fc67005acf8dbf9a0"
        key = "f665464fa637"
        instance = LowMCInstance(360, 60, 12, AES_SBOX, 40, field=field)
        enc, _, _ = encrypt(message, key, instance)
        dec = decrypt(enc, key, instance)
        self.assertEqual(message, dec)


if __name__ == '__main__':
    unittest.main()
