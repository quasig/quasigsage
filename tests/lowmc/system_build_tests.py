from sage.all import *

import unittest

from quasigsage.ciphers.aes.constants import AES_SBOX
from quasigsage.ciphers.lowmc import lowmc
from quasigsage.ciphers.lowmc.constants import LOWMC_SBOX
from quasigsage.ciphers.lowmc.lowmc import LowMCInstance
from quasigsage.helpers import hex_to_finite_field
from quasigsage.signatures.lowmc.lowmc import create_spn_system, create_mrhs_system


class LowMcMrhsSystemTest(unittest.TestCase):

    def setUp(self):
        self.plain_text = "f3a4a36ff30fc67005acf8dbf9a0fc9313a4a36ff30fc67005acf8dbf9a0fc93"
        self.key = "f665464fa6373153079f"
        self.instance_gf2 = LowMCInstance(256, 80, 12, LOWMC_SBOX, 16)
        self.instance_gf16 = LowMCInstance(256, 80, 12, AES_SBOX, 16, field=GF(2 ** 4))

    def test_if_x_times_spn_system_in_gf2_are_sbox_ins_and_outs(self):
        sbox_size = self.instance_gf2.sbox_size / self.instance_gf2.field.degree() * self.instance_gf2.number_of_sboxes

        spn_system = create_spn_system(self.key, self.instance_gf2)

        initial = hex_to_finite_field(self.plain_text, self.instance_gf2.field, fill_bits=self.instance_gf2.block_size)
        _, u_in, u_out = lowmc.encrypt(self.plain_text, self.key, self.instance_gf2)

        ins = [item for t in u_in for item in t]
        outs = [item for t in u_out for item in t]

        x = vector(self.instance_gf2.field, initial.list() + outs + [1])
        actual_result = (x * spn_system).list()

        expected_result = []
        for i in range(0, len(outs), sbox_size):
            expected_result += ins[i: i + sbox_size]
            expected_result += outs[i: i + sbox_size]

        self.assertListEqual(expected_result, actual_result)

    def test_if_x_times_spn_system_in_gf16_are_sbox_ins_and_outs(self):
        sbox_size = self.instance_gf16.sbox_size / self.instance_gf16.field.degree() * self.instance_gf16.number_of_sboxes

        spn_system = create_spn_system(self.key, self.instance_gf16)

        initial = hex_to_finite_field(self.plain_text, self.instance_gf16.field,
                                      fill_bits=self.instance_gf16.block_size)
        _, u_in, u_out = lowmc.encrypt(self.plain_text, self.key, self.instance_gf16)

        ins = [item for t in u_in for item in t]
        outs = [item for t in u_out for item in t]

        x = vector(self.instance_gf16.field, initial.list() + outs + [1])
        actual_result = (x * spn_system).list()

        expected_result = []
        for i in range(0, len(outs), sbox_size):
            expected_result += ins[i: i + sbox_size]
            expected_result += outs[i: i + sbox_size]

        self.assertListEqual(expected_result, actual_result)


    def test_if_x_times_mrhs_system_in_gf2_are_sbox_ins_and_outs(self):
        sbox_size = self.instance_gf2.sbox_size / self.instance_gf2.field.degree()

        spn_system = create_spn_system(self.key, self.instance_gf2)
        mrhs_system = create_mrhs_system(spn_system, self.instance_gf2)

        initial = hex_to_finite_field(self.plain_text, self.instance_gf2.field, fill_bits=self.instance_gf2.block_size)
        _, u_in, u_out = lowmc.encrypt(self.plain_text, self.key, self.instance_gf2)
        ins = [item for t in u_in for item in t]
        outs = [item for t in u_out for item in t]

        x = vector(self.instance_gf2.field, initial.list() + outs + [1])
        actual_result = (x * mrhs_system).list()

        expected_result = []
        for i in range(0, len(outs), sbox_size):
            expected_result += ins[i: i + sbox_size]
            expected_result += outs[i: i + sbox_size]

        self.assertListEqual(expected_result, actual_result)

    def test_if_x_times_mrhs_system_in_gf16_are_sbox_ins_and_outs(self):
        sbox_size = self.instance_gf16.sbox_size / self.instance_gf16.field.degree()

        spn_system = create_spn_system(self.key, self.instance_gf16)
        mrhs_system = create_mrhs_system(spn_system, self.instance_gf16)

        initial = hex_to_finite_field(self.plain_text, self.instance_gf16.field,
                                      fill_bits=self.instance_gf16.block_size)
        _, u_in, u_out = lowmc.encrypt(self.plain_text, self.key, self.instance_gf16)

        ins = [item for t in u_in for item in t]
        outs = [item for t in u_out for item in t]

        x = vector(self.instance_gf16.field, initial.list() + outs + [1])
        actual_result = (x * mrhs_system).list()

        expected_result = []
        for i in range(0, len(outs), sbox_size):
            expected_result += ins[i: i + sbox_size]
            expected_result += outs[i: i + sbox_size]

        self.assertListEqual(expected_result, actual_result)


if __name__ == '__main__':
    unittest.main()
