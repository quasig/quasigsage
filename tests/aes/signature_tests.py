import unittest
import logging

import quasigsage.signatures.aes.aes_gf as gf
import quasigsage.signatures.aes.aes_bin as bin

logging.getLogger().setLevel(logging.DEBUG)


class AesSignatureTest(unittest.TestCase):

    def setUp(self):
        self.message = "Some test message"
        self.different_message = "Some test massage"

    def test_gf_valid_signature(self):
        public_key, private_key = gf.key_generation()
        signature = gf.sign(self.message, private_key)
        valid = gf.verify(self.message, signature, public_key)
        self.assertEqual(True, valid)

    def test_gf_invalid_signature_of_different_message(self):
        public_key, private_key = gf.key_generation()
        signature = gf.sign(self.message, private_key)
        valid = gf.verify(self.different_message, signature, public_key)
        self.assertEqual(False, valid)

    def test_bin_valid_signature(self):
        public_key, private_key = bin.key_generation()
        signature = bin.sign(self.message, private_key)
        valid = bin.verify(self.message, signature, public_key)
        self.assertEqual(True, valid)

    def test_bin_invalid_signature_of_different_message(self):
        public_key, private_key = bin.key_generation()
        signature = bin.sign(self.message, private_key)
        valid = bin.verify(self.different_message, signature, public_key)
        self.assertEqual(False, valid)


if __name__ == '__main__':
    unittest.main()
